# Nexus
#types of nexus repository
In Nexus Repository Manager 3 (NXRM3) there are three repository types—proxy repositories, hosted repositories, and repository groups—all using a number of different repository formats. Understanding the available repository types helps define what is needed within your organization for a successful NXRM3 implementation.
For example, if your team needs to access public repositories, you'll want to create proxy repositories. If your team has components that aren’t public, but are used by others in your development organization, then creating a hosted repository is the way to go. If you have multiple repositories that would be easier to access from a single URL, then creating a group repository fits that need. 
